#!/usr/bin/env python3

# (c) 2018-2021 Christian Henz
# SPDX-License-Identifier: GPL-2.0-or-later

"""
A stupid Amiga HUNK linker, designed to be compatible with BLINK.
"""

import codecs
import logging
import re
import sys

class EOF(Exception):
    pass

class ShortRead(Exception):
    pass

class XRef:
    def __init__(self, type_, offset, unoptimizable):
        self.type = type_
        self.offset = offset
        self.unoptimizable = unoptimizable

    @property
    def size(self):
        return XREF_SIZES[self.type]

class Hunk(object):
    def __init__(self, num):
        self.num = num
        self.type = 0
        self.mem_flags = 0
        self.name = ""
        self.offset = 0
        self.size = 0 # bytes
        self.data = bytearray()
        self.relocs = {}
        self.xdefs = {}
        self.xrefs = {}
        self.next = None

    def dump(self, file = sys.stderr):
        def print(s):
            file.write("{}\n".format(s))
        if self.offset:
            print('hunk #{} - offset {:08x}'.format(self.num, self.offset))
        else:
            print('hunk #{} - "{}":'.format(self.num, self.name))
            print(" - type: {}, mem {:04x}".format(hunk_name(self.type), self.mem_flags >> 16))
        if self.size:
            print(" - size: {:08x} bytes".format(self.size))
        if self.relocs:
            print(" - relocs:")
            for type, relocs in self.relocs.items():
                print("   - type {}".format(reloc_name(type)))
                for hunk, offsets in sorted(relocs.items()):
                    print("     - into hunk #{}".format(hunk))
                    for offset, _ in sorted(offsets):
                        num_bytes = reloc_size(type)
                        print("       - @{:08x}: {}".format(offset + self.offset, self.data[offset : offset + num_bytes].hex()))
        if self.xdefs:
            print(" - xdefs:")
            for name, value in sorted(self.xdefs.items()):
                print('   - "{}": {:08x}'.format(name, value + self.offset))
        if self.xrefs:
            print(" - xrefs:")
            for name, xrefs in sorted(self.xrefs.items()):
                print('   - "{}":'.format(name))
                for xref in xrefs:
                    print('     - @{:08x} ({})'.format(xref.offset + self.offset, ext_name(xref.type)))

    def add_xdef(self, type, name, value):
        if type != EXT_DEF:
            raise NotImplementedError("xdef type {}".format(ext_name(type)))
        self.xdefs[name] = value
        #logging.info("XDEF {}".format(name))

    def add_xrefs(self, name, xrefs):
        assert not name in self.xrefs
        self.xrefs[name] = xrefs

    def add_relocs(self, type, hunk_num, offsets):
        relocs = self.relocs.setdefault(type, {})
        relocs[hunk_num] = offsets

    def add_reloc(self, type, hunk_num, offset):
        relocs = self.relocs.setdefault(type, {})
        offsets = relocs.setdefault(hunk_num, [])
        offsets.append((offset,0))

    def append(self, hunk):
        """
        Append a hunk to this hunk, and change the number and offset
        accordingly.
        """
        assert self.name == hunk.name, "name mismatch"
        assert self.type == hunk.type, "type mismatch"
        assert self.mem_flags == hunk.mem_flags, "mem flags mismatch"
        curr = self
        while curr.next:
            curr = curr.next
        curr.next = hunk
        hunk.offset = curr.offset + curr.size
        hunk.num = curr.num

    def calc_size(self):
        s = 0
        h = self
        while h:
            s += h.size
            h = h.next
        return s

    def adjust_relocs(self, old_num, new_num, new_base):
        """
        Adjust relocs after a hunk number has changed.
        """
        for type, relocs in self.relocs.items():
            for offset, _ in relocs.pop(old_num, []):
                self.add_reloc(type, new_num, offset)
                if type not in (HUNK_RELOC32, HUNK_RELOC16, HUNK_RELOC8):
                    raise NotImplementedError("reloc type {}".format(hunk_name(type)))
                num_bytes = reloc_size(type)
                value = parse_word(self.data[offset : offset + num_bytes])
                self.data[offset : offset + num_bytes] = fmt_word(new_base + value, num_bytes)

    def write_loadfile(self, f):
        f.write(fmt_l(self.mem_flags | self.type))
        f.write(fmt_l((self.calc_size() + 3) // 4))
        h = self
        while h:
            if len(h.data):
                f.write(h.data)
            h = h.next
        h = self
        while h:
            for type, relocs in h.relocs.items():
                f.write(fmt_l(type))
                for num, offsets in sorted(relocs.items()):
                    f.write(fmt_l(len(offsets)))
                    f.write(fmt_l(num))
                    for offset,_ in sorted(offsets):
                        f.write(fmt_l(offset + h.offset))
                f.write(fmt_l(0))
            h = h.next
        f.write(fmt_l(HUNK_END))

def read(f, l):
    bs = f.read(l)
    if len(bs) == 0:
        raise EOF()
    elif len(bs) != l:
        raise ShortRead()
    return bs

def read_word(f, l):
    bs = read(f, l)
    return int(bs.hex(), 16)

def read_l(f):
    return read_word(f, 4)

def read_w(f):
    return read_word(f, 2)

def read_b(f):
    return read_word(f, 1)

def read_zs(f, l):
    bs = read(f, l)
    return codecs.decode(bs, "ascii").rstrip("\0")

HUNK_UNIT = 0x03e7
HUNK_NAME = 0x03e8
HUNK_CODE = 0x03e9
HUNK_DATA = 0x03ea
HUNK_BSS = 0x03eb
HUNK_RELOC32 = 0x03ec
HUNK_RELOC16 = 0x03ed
HUNK_RELOC8 = 0x03ee
HUNK_EXT = 0x03ef
HUNK_SYMBOL = 0x03f0
HUNK_END = 0x03f2
HUNK_HEADER = 0x03f3

HUNK_NAMES = {
    HUNK_UNIT: "HUNK_UNIT",
    HUNK_NAME: "HUNK_NAME",
    HUNK_CODE: "HUNK_CODE",
    HUNK_DATA: "HUNK_DATA",
    HUNK_BSS: "HUNK_BSS",
    HUNK_RELOC32: "HUNK_RELOC32",
    HUNK_RELOC16: "HUNK_RELOC16",
    HUNK_RELOC8: "HUNK_RELOC8",
    HUNK_EXT: "HUNK_EXT",
    HUNK_SYMBOL: "HUNK_SYMBOL",
    HUNK_END: "HUNK_END",
    HUNK_HEADER: "HUNK_HEADER",
}

def hunk_name(type):
    return HUNK_NAMES.get(type, "0x{:04x}".format(type))

def reloc_size(type):
    return {
        HUNK_RELOC32: 4,
        HUNK_RELOC16: 2,
        HUNK_RELOC8: 1
    }[type]

def reloc_name(type):
    return hunk_name(type)

# from Amiga DOS manual
EXT_SYMB      = 0      # symbol table
EXT_DEF	      = 1      # relocatable definition
EXT_ABS	      = 2      # Absolute definition
EXT_RES	      = 3      # no longer supported
EXT_ABSREF32  = 129    # 32 bit reference to symbol
EXT_ABSCOMMON = 130    # 32 bit reference to COMMON block
EXT_RELREF16  = 131    # 16 bit reference to symbol
EXT_RELREF8   = 132    #  8 bit reference to symbol
# from doshunks.h, OS 2.04
EXT_DEXT32    = 133    # 32 bit data releative reference
EXT_DEXT16    = 134    # 16 bit data releative reference
EXT_DEXT8     = 135    #  8 bit data releative reference
# from doshunks.h, OS 3.9
EXT_RELREF32  = 136    # 32 bit PC-relative reference to symbol
EXT_RELCOMMON = 137    # 32 bit PC-relative reference to COMMON block
EXT_ABSREF16  = 138    # 16 bit absolute reference to symbol
EXT_ABSREF8   = 139    # 8 bit absolute reference to symbol

XREF_SIZES = {
    EXT_ABSREF32: 4,
    EXT_ABSCOMMON: 4,
    EXT_RELREF16: 2,
    EXT_RELREF8: 1,
    EXT_DEXT32: 4,
    EXT_DEXT16: 2,
    EXT_DEXT8: 1,
    EXT_RELREF32: 4,
    EXT_RELCOMMON: 4,
    EXT_ABSREF16: 2,
    EXT_ABSREF8: 1,
}

EXT_NAMES = {
    EXT_SYMB: "EXT_SYMB",
    EXT_DEF: "EXT_DEF",
    EXT_ABS: "EXT_ABS",
    EXT_RES: "EXT_RES",
    EXT_ABSREF32: "EXT_ABSREF32",
    EXT_ABSCOMMON: "EXT_ABSCOMMON",
    EXT_RELREF16: "EXT_RELREF16",
    EXT_RELREF8: "EXT_RELREF8",
    EXT_DEXT32: "EXT_DEXT32",
    EXT_DEXT16: "EXT_DEXT16",
    EXT_DEXT8: "EXT_DEXT8",
    EXT_RELREF32: "EXT_RELREF32",
    EXT_RELCOMMON: "EXT_RELCOMMON",
    EXT_ABSREF16: "EXT_ABSREF16",
    EXT_ABSREF8: "EXT_ABSREF8",
}

def ext_name(type):
    return EXT_NAMES.get(type, str(type))

MEMF_CHIP = 0x40000000
MEMF_FAST = 0x80000000

# a flag used by the TIGCC version of A68k
UNOPTIMIZABLE = 0x80000000

def iter_hunks(hunks):
    for hunk in hunks:
        while hunk:
            yield hunk
            hunk = hunk.next

def load_header(f, *, debug_dump = False):
    while 1:
        name_len = read_l(f) * 4
        if name_len == 0:
            break
        name = read_zd(f, name_len)
        logging.debug("resident library: {}".format(n))
    table_size = read_l(f)
    first_hunk_num = read_l(f)
    last_hunk_num = read_l(f)
    for _ in range(table_size):
        read_l(f)
    return first_hunk_num

def load_f(f, hunks, global_hunk_num, *, debug_dump = False):
    block_type = read_l(f)
    if block_type == HUNK_HEADER:
        global_hunk_num = load_header(f, debug_dump = debug_dump) - 1
    elif block_type == HUNK_UNIT:
        name_len = read_l(f) * 4
        if name_len > 0:
            unit_name = read_zs(f, name_len)
            #logging.debug("UNIT: {}".format(unit_name))
    else:
        raise Exception("Expected HUNK_UNIT, got {}".format(hunk_name(block_type)))

    hunk = None
    hunk_num = -1
    while 1:
        try:
            block_type = read_l(f)
            if not hunk:
                hunk_num += 1
                global_hunk_num += 1
                #logging.debug("-- hunk #{} (global #{})".format(hunk_num, global_hunk_num))
                hunk = Hunk(global_hunk_num)
            mem_flags = block_type & 0xffff0000
            hunk.mem_flags |= mem_flags
            mem_types = []
            if mem_flags == MEMF_CHIP:
                mem_types.append("CHIP")
            elif mem_flags == MEMF_FAST:
                mem_types.append("FAST")
            elif mem_flags != 0:
                raise NotImplementedError("memory flag {:08x}".format(mem_flags))
            block_type = block_type & 0xffff
            if mem_types:
                #logging.debug("MEM: {}".format(",".join(mem_types)))
                pass
            if block_type == HUNK_NAME:
                name_len = read_l(f) * 4
                hunk.name = read_zs(f, name_len)
                #logging.debug("NAME: {}".format(hunk.name))
            elif block_type in (HUNK_CODE, HUNK_DATA):
                hunk.type = block_type
                hunk.size = read_l(f) * 4
                hunk.data = bytearray(read(f, hunk.size))
                #logging.debug("{}: {} bytes".format("CODE" if block_type == HUNK_CODE else "DATA", hunk.size))
            elif block_type == HUNK_BSS:
                hunk.type = block_type
                hunk.size = read_l(f) * 4
                #logging.debug("BSS: {} bytes".format(hunk.size))
            elif block_type in (HUNK_EXT, HUNK_SYMBOL):
                #logging.debug("EXT:")
                while 1:
                    sym_type = read_b(f)
                    name_len = read_word(f, 3) * 4
                    if name_len:
                        sym_name = read_zs(f, name_len)
                        #logging.debug("- {:02x} {}".format(sym_type, sym_name))
                        if sym_type in (EXT_SYMB, EXT_DEF, EXT_ABS, EXT_RES):
                            sym_data = read_l(f)
                            #logging.debug("  {:08x}".format(sym_data))
                            # TODO: support for symbol table
                            if sym_type != EXT_SYMB:
                                hunk.add_xdef(sym_type, sym_name, sym_data)
                        elif sym_type in (EXT_ABSREF32, EXT_RELREF16, EXT_RELREF8, EXT_DEXT32, EXT_DEXT16, EXT_DEXT8, EXT_RELREF32, EXT_ABSREF16, EXT_ABSREF8):
                            num_refs = read_l(f)
                            xrefs = []
                            for i in range(num_refs):
                                ref = read_l(f)
                                unoptimizable = ref & UNOPTIMIZABLE
                                ref = ref & ~UNOPTIMIZABLE
                                #logging.debug("  - {:08x}".format(ref) + (" (unoptimizable)" if unoptimizable else ""))
                                xrefs.append(XRef(sym_type, ref, unoptimizable))
                            hunk.add_xrefs(sym_name, xrefs)
                        elif sym_type == EXT_ABSCOMMON:
                            common_size = read_l(f)
                            #logging.debug("  common size {:08x}".format(common_size))
                            num_refs = read_l(f)
                            for i in range(num_refs):
                                ref = read_l(f)
                                #logging.debug("  - {:08x}".format(ref))
                            raise NotImplementedError("EXT_ABSCOMMON")
                        else:
                            raise Exception("unexpected symbol type {} (name: {})".format(ext_name(sym_type), sym_name))
                    else:
                        break
            elif block_type == HUNK_RELOC32:
                #logging.debug("RELOC32:")
                while 1:
                    num_relocs = read_l(f)
                    if num_relocs == 0:
                        break
                    reloc_hunk_num = read_l(f)
                    global_reloc_hunk_num = reloc_hunk_num + global_hunk_num - hunk_num
                    #logging.debug("- hunk #{} (global #{})".format(reloc_hunk_num, global_reloc_hunk_num))
                    relocs = []
                    for i in range(num_relocs):
                        offset = read_l(f)
                        unoptimizable = offset & UNOPTIMIZABLE
                        offset = offset & ~UNOPTIMIZABLE
                        #logging.debug("  - {:08x}".format(offset) + (" (unoptimizable)" if unoptimizable else ""))
                        relocs.append((offset, unoptimizable))
                    hunk.add_relocs(block_type, global_reloc_hunk_num, relocs)
            elif block_type == HUNK_END:
                if debug_dump:
                    hunk.dump()
                hunks.append(hunk)
                hunk = None
            else:
                raise NotImplementedError("block type {}".format(hunk_name(block_type)))
                break
        except EOF:
            if hunk:
                raise Exception("unfinished hunk")
            break

def link(fns, o="", *, debug_dump = False):
    hunks = []
    for fn in fns:
        with open(fn, "rb") as f:
            logging.info("loading file {}...".format(fn))
            load_f(f, hunks, len(hunks) - 1, debug_dump = debug_dump)
    logging.info("loaded {} hunks".format(len(hunks)))
    logging.info("merging hunks...")
    merge_hunks(hunks)
    logging.info("{} hunks left".format(len(hunks)))
    logging.info("resolving externals...")
    resolve_exts(hunks)
    if debug_dump:
        for h in iter_hunks(hunks):
            h.dump()
    if o:
        with open(o, "wb+") as f:
            write_loadfile(hunks, f)

def fmt_word(val, n):
    num_bits = n * 8
    assert n < 2 ** num_bits
    assert n >= -(2 ** (num_bits - 1))
    t = "{:0" + str(2 * n) + "x}"
    mask = (2 ** num_bits) - 1
    return codecs.decode(t.format(val & mask), "hex")

def fmt_l(val):
    return fmt_word(val, 4)

def parse_word(bs, n = 0):
    n = n or len(bs)
    assert len(bs) == n
    return int(bs.hex(), 16)

def resolve_exts(hunks):
    # loop over xrefs
    for rh in hunks:
        while rh:
            #logging.debug("resolving xrefs for hunk {}".format(rh.num))
            for name, xrefs in rh.xrefs.items():
                #logging.debug("- {}".format(name))
                # loop over xdefs
                for dh in hunks:
                    found = False
                    while dh:
                        value = dh.xdefs.get(name)
                        if value is not None:
                            # value is offset into dh
                            for xref in xrefs:
                                if xref.type == EXT_ABSREF32:
                                    reloc_type = HUNK_RELOC32
                                elif xref.type in (EXT_RELREF16, EXT_RELREF8):
                                    reloc_type = None
                                else:
                                    raise NotImplementedError("xref type {}".format(ext_name(xref.type)))
                                # in rh, write `value` at `offset`
                                # in rh, add RELOC32 for offset with dh's hunk number
                                current_value = parse_word(rh.data[xref.offset : xref.offset + xref.size])
                                logging.debug("-- @{:08x} -> {:02}:{:08x}".format(rh.offset + xref.offset, dh.num, dh.offset + value))
                                mask = (2 ** (8 * xref.size)) - 1
                                # FIXME: check for overflow?
                                value_to_write = (current_value + (dh.offset + value)) & mask
                                rh.data[xref.offset : xref.offset + xref.size] = fmt_word(value_to_write, xref.size)
                                if reloc_type:
                                    rh.add_reloc(reloc_type, dh.num, xref.offset)
                                found = True
                            break
                        dh = dh.next
                    if found:
                        break
                else:
                    raise Exception("unresolved external: {}".format(name))
            rh.xrefs.clear()
            rh = rh.next

def merge_hunks(hunks):
    # chain all matching hunks together
    # [a1, b1, a2, c1] --> [(a1->a2), b1, c1]
    i = 0
    while i < len(hunks):
        lh = hunks[i]
        j = i + 1
        while j < len(hunks):
            rh = hunks[j]
            if rh.type == lh.type and rh.mem_flags == lh.mem_flags and rh.name == lh.name:
                old_num = rh.num
                lh.append(rh)
                hunks.pop(j)
                for h in iter_hunks(hunks):
                    h.adjust_relocs(old_num, rh.num, rh.offset)
            else:
                j += 1
        i += 1

def write_loadfile(hunks, f):
    f.write(fmt_l(HUNK_HEADER))
    f.write(fmt_l(0))
    f.write(fmt_l(len(hunks)))
    f.write(fmt_l(0))
    f.write(fmt_l(len(hunks) - 1))
    for h in hunks:
        f.write(fmt_l(h.mem_flags | (h.calc_size() + 3) // 4))
    for h in hunks:
        h.write_loadfile(f)

class UserError(Exception):
    pass

def _parse_commandline(args, options):

    ACCEPTING_MORE = 1
    COMPLETE = 2

    def parse_list(key):
        class P:
            def __init__(self, keyword):
                self.keyword = keyword
                self.status = ACCEPTING_MORE
            def __call__(self, s):
                parts = re.split("[ ,+]", s)
                options.setdefault(key, []).extend(parts)
                self.status |= COMPLETE
        return P

    def parse_string(key):
        class P:
            def __init__(self, keyword):
                self.keyword = keyword
                self.status = ACCEPTING_MORE
            def __call__(self, word):
                options[key] = word
                self.status = COMPLETE
        return P

    def parse_true(key):
        class P:
            def __init__(self, _):
                options[key] = True
                self.status = COMPLETE
        return P

    class ParseWith:
        def __init__(self, keyword):
            self.keyword = keyword
            self.status = ACCEPTING_MORE
        def __call__(self, word):
            with open(word, "rb") as f:
                s = f.read().decode("latin1").strip()
                args = re.split("\\s+", s)
                _parse_commandline(args, options)
                self.status = COMPLETE

    keywords = {
        "from": parse_list("from"),
        "root": parse_list("from"),
        "library": parse_list("lib"),
        "lib": parse_list("lib"),
        "to": parse_string("to"),
        "with": ParseWith,
        "ver": parse_string("ver"),
        "xref": parse_string("xref"),
        "faster": parse_true("faster"),
        "verbose": parse_true("verbose"),
        "nodebug": parse_true("nodebug"),
        "smalldata": parse_true("smalldata"),
        "smallcode": parse_true("smallcode"),
        "-h": parse_true("help"),
        "--help": parse_true("help"),
    }

    p = None
    for i, arg in enumerate(args):
        if arg.lower() in keywords:
            if p and not (p.status & COMPLETE):
                raise UserError("{} requires argument".format(p.keyword.upper()))
            p = keywords[arg.lower()](arg)
        else:
            if not p:
                if i == 0:
                    p = parse_list("from")("FROM")
                else:
                    raise UserError("unknown option '{}'".format(arg))
            if not (p.status & ACCEPTING_MORE):
                raise UserError("{} accepts no further arguments".format(p.keyword.upper()))
            p(arg)

def parse_commandline(args):
    options = {}
    _parse_commandline(args, options)
    return options

usage = """usage: hunky.py [options]

   FROM <files>     object files
   TO <file>        executable file
   -h | --help      this text
"""

def main():
    logging.basicConfig(level = logging.INFO, format = "%(levelname)s: %(message)s")
    try:
        options = parse_commandline(sys.argv[1:])
    except UserError as e:
        logging.exception(e)
        sys.exit(1)
    if options.get("help"):
        print(usage)
        sys.exit(0)
    if not options.get("to"):
        logging.error("no output file name specified")
        sys.exit(1)
    if not options.get("from"):
        logging.error("no input files specified")
        sys.exit(1)
    for k in options.keys():
        if k in ("to", "from", "verbose"):
            continue
        logging.warning("ignored option: {}".format(k))
    if options.get("verbose"):
        logging.getLogger().setLevel(logging.DEBUG)
    try:
        link(options.get("from"), o = options.get("to"), debug_dump = options.get("verbose"))
    except Exception as e:
        logging.exception(e)
        sys.exit(1)

if __name__ == "__main__":
    main()
